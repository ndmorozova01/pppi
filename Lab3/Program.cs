﻿var randomGenerator = new Random();

// Task 1
var task1 = new Thread(DoPrint);
task1.Start();

// Task 2
Console.WriteLine(await GetText());

void DoPrint() => Console.WriteLine("Hello world");

Task<string> GetText() => Task.FromResult("Answer #2");

// Task 3 & 4
var results = await Task.WhenAll(GetNumber(randomGenerator, 10, 20), GetNumber(randomGenerator, 21, 30),
    GetNumber(randomGenerator, 31, 40));

foreach (var result in results)
{
    Console.WriteLine(result);
}

Task<int> GetNumber(Random random, int min, int max) => Task.FromResult(random.Next(min, max));