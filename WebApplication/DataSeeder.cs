using WebApplication.Models.Authorization;
using WebApplication.Models.Movies;
using WebApplication.Models.Pets;
using WebApplication.Models.Todos;

namespace WebApplication;

public static class DataSeeder
{
    public static readonly IEnumerable<Todo> Todos = new List<Todo>
    {
        new()
        {
            Id = 1, Title = "Create Web App", CreateDate = new DateTime(2022, 10, 10, 12, 30, 45), IsDone = false
        },
        new()
        {
            Id = 2, Title = "Add Authorization", CreateDate = new DateTime(2022, 10, 10, 12, 35, 45), IsDone = false
        }
    };

    public static readonly IEnumerable<Pet> Pets = new List<Pet>
    {
        new()
        {
            Id = 1, Name = "Murzik", FavouriteToy = "Kesha", DateOfBirth = new DateOnly(2020, 10, 18)
        },
        new()
        {
            Id = 2, Name = "Kesha", FavouriteToy = "Murzik", DateOfBirth = new DateOnly(2021, 11, 1)
        }
    };

    public static readonly IEnumerable<Movie> Movies = new List<Movie>
    {
        new()
        {
            Id = 1, Genre = Genre.Drama, Title = "The Shawshank Redemption", ReleasedAt = new DateOnly(1994, 9, 10)
        },
        new()
        {
            Id = 2, Genre = Genre.ScienceFiction, Title = "Gattaca", ReleasedAt = new DateOnly(1997, 10, 24)
        },
        new()
        {
            Id = 3, Genre = Genre.History, Title = "The King's Speech", ReleasedAt = new DateOnly(2010, 12, 23)
        }
    };

    public static readonly IEnumerable<User> Users = new List<User>
    {
        new()
        {
            Email = "test1@gmail.com",
            Name = "Test 1",
            Password = BCrypt.Net.BCrypt.HashPassword("admin"),
            Surname = "Surname 1",
            DateOfBirth = new(2000, 10, 10)
        },
        new()
        {
            Email = "test2@gmail.com",
            Name = "Test 2",
            Password = BCrypt.Net.BCrypt.HashPassword("root"),
            Surname = "Surname 2",
            DateOfBirth = new(2000, 10, 10)
        }
    };
}