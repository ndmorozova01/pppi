using Microsoft.EntityFrameworkCore;
using OneOf;
using WebApplication.Models.Todos;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class TodoService : ITodoService
{
    private readonly ApplicationDbContext _context;

    public TodoService(ApplicationDbContext context) => _context = context;

    public async Task<IEnumerable<Todo>> GetAllAsync() => await _context.Todos.AsNoTracking().ToListAsync();

    public async Task<Todo> AddTodoAsync(AddTodo addTodo)
    {
        var todo = new Todo
        {
            Title = addTodo.Title,
            CreateDate = addTodo.CreateDate,
            IsDone = addTodo.IsDone
        };

        _context.Todos.Add(todo);

        await _context.SaveChangesAsync();

        return todo;
    }

    public async Task<OneOf<TodoNotFound, Todo>> UpdateTodoAsync(int id, UpdateTodo updateTodo)
    {
        var todo = await _context.Todos.FindAsync(id);

        if (todo == null)
        {
            return new TodoNotFound { Id = id };
        }

        if (updateTodo.CreateDate.HasValue && todo.CreateDate != updateTodo.CreateDate.Value)
        {
            todo.CreateDate = updateTodo.CreateDate.Value;
        }

        if (updateTodo.IsDone.HasValue && todo.IsDone != updateTodo.IsDone.Value)
        {
            todo.IsDone = updateTodo.IsDone.Value;
        }

        if (updateTodo.Title != null && todo.Title != updateTodo.Title)
        {
            todo.Title = updateTodo.Title;
        }

        await _context.SaveChangesAsync();

        return todo;
    }

    public async Task<OneOf<TodoNotFound, bool>> DeleteTodoAsync(int id)
    {
        var todo = await _context.Todos.FindAsync(id);

        if (todo == null)
        {
            return new TodoNotFound { Id = id };
        }

        var result = _context.Todos.Remove(todo);

        await _context.SaveChangesAsync();

        return result.State == EntityState.Deleted;
    }
}