using Microsoft.EntityFrameworkCore;
using OneOf;
using WebApplication.Models.Movies;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class MoviesService : IMoviesService
{
    private readonly ApplicationDbContext _context;
    
    public MoviesService(ApplicationDbContext context) => _context = context;

    public async Task<IEnumerable<Movie>> GetAllAsync() => await _context.Movies.AsNoTracking().ToListAsync();

    public async Task<Movie> AddMovieAsync(AddMovie addMovie)
    {
        var movie = new Movie
        {
            Genre = addMovie.Genre,
            Title = addMovie.Title,
            ReleasedAt = addMovie.ReleasedAt
        };

        _context.Movies.Add(movie);
        await _context.SaveChangesAsync();

        return movie;
    }

    public async Task<OneOf<MovieNotFound, Movie>> UpdateMovieAsync(int id, UpdateMovie updateMovie)
    {
        var movie = await _context.Movies.FindAsync(id);

        if (movie == null)
        {
            return new MovieNotFound { Id = id };
        }

        if (updateMovie.ReleasedAt.HasValue && movie.ReleasedAt != updateMovie.ReleasedAt.Value)
        {
            movie.ReleasedAt = updateMovie.ReleasedAt.Value;
        }

        if (updateMovie.Genre.HasValue && movie.Genre != updateMovie.Genre.Value)
        {
            movie.Genre = updateMovie.Genre.Value;
        }

        if (updateMovie.Title != null && movie.Title != updateMovie.Title)
        {
            movie.Title = updateMovie.Title;
        }

        await _context.SaveChangesAsync();
        
        return movie;
    }

    public async Task<OneOf<MovieNotFound, bool>> DeleteMovieAsync(int id)
    {
        var movie = await _context.Movies.FindAsync(id);

        if (movie == null)
        {
            return new MovieNotFound { Id = id };
        }

        var result = _context.Movies.Remove(movie);

        await _context.SaveChangesAsync();

        return result.State == EntityState.Deleted;
    }
}