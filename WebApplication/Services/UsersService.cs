using Microsoft.EntityFrameworkCore;
using WebApplication.Models.Authorization;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class UsersService : IUsersService
{
    private readonly ApplicationDbContext _context;

    public UsersService(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<User?> GetByEmail(string email) =>
        await _context.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Email == email);
}