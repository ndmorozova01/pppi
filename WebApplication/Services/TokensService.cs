using Microsoft.EntityFrameworkCore;
using OneOf;
using WebApplication.Models.Authorization;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class TokensService : ITokensService
{
    private readonly ApplicationDbContext _context;

    public TokensService(ApplicationDbContext context) => _context = context;

    public async Task AddTokensAsync(Tokens tokens)
    {
        await _context.Tokens.AddAsync(tokens);
        await _context.SaveChangesAsync();
    }

    public async Task<Tokens?> GetTokensAsync(string refreshToken) =>
        await _context.Tokens.FirstOrDefaultAsync(r => r.RefreshToken == refreshToken);

    public async Task<Tokens> UpdateTokensAsync(Tokens tokens)
    {
        _context.Tokens.Entry(tokens).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return tokens;
    }

    public async Task<OneOf<InvalidRefreshToken, bool>> RemoveTokens(string refreshToken)
    {
        var tokens = await _context.Tokens.FirstOrDefaultAsync(r => r.RefreshToken == refreshToken);

        if (tokens == null)
        {
            return new InvalidRefreshToken { RefreshToken = refreshToken };
        }

        var result = _context.Tokens.Remove(tokens);
        await _context.SaveChangesAsync();

        return result.State == EntityState.Deleted;
    }
}