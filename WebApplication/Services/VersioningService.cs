using ClosedXML.Excel;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class VersioningService : IVersioningService
{
    public int GetV1() => 1;

    public string GetV2() => "Hello world";

    public Stream GetV3()
    {
        var workbook = new XLWorkbook();
        var worksheet = workbook.Worksheets.Add("Sample Sheet");
        worksheet.Cell("A1").Value = "Hello World!";
        worksheet.Cell("A2").FormulaA1 = "=MID(A1, 7, 5)";

        var memoryStream = new MemoryStream();
        workbook.SaveAs(memoryStream);
        memoryStream.Position = 0;
        return memoryStream;
    }
}