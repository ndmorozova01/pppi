namespace WebApplication.Services.Interfaces;

public interface IPasswordService
{
    bool VerifyPassword(string password, string hash);
}