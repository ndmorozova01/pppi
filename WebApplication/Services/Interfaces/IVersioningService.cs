namespace WebApplication.Services.Interfaces;

public interface IVersioningService
{
    int GetV1();
    
    string GetV2();

    Stream GetV3();
}