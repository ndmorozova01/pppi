using OneOf;
using WebApplication.Models.Authorization;

namespace WebApplication.Services.Interfaces;

public interface ITokensService
{
    Task AddTokensAsync(Tokens tokens);

    Task<Tokens?> GetTokensAsync(string refreshToken);
    
    Task<Tokens> UpdateTokensAsync(Tokens tokens);

    Task<OneOf<InvalidRefreshToken, bool>> RemoveTokens(string refreshToken);
}