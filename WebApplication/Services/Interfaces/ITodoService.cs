using OneOf;
using WebApplication.Models.Todos;

namespace WebApplication.Services.Interfaces;

public interface ITodoService
{
    Task<IEnumerable<Todo>> GetAllAsync();

    Task<Todo> AddTodoAsync(AddTodo addMovie);

    Task<OneOf<TodoNotFound, Todo>> UpdateTodoAsync(int id, UpdateTodo updateMovie);

    Task<OneOf<TodoNotFound, bool>> DeleteTodoAsync(int id);
}