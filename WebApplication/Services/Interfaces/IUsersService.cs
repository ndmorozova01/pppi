using WebApplication.Models.Authorization;

namespace WebApplication.Services.Interfaces;

public interface IUsersService
{
    Task<User?> GetByEmail(string email);
}