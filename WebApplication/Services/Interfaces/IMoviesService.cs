using WebApplication.Models.Movies;

namespace WebApplication.Services.Interfaces;

public interface IMoviesService
{
    Task<IEnumerable<Movie>> GetAllAsync();

    Task<Movie> AddMovieAsync(AddMovie addMovie);

    Task<OneOf.OneOf<MovieNotFound, Movie>> UpdateMovieAsync(int id, UpdateMovie updateMovie);

    Task<OneOf.OneOf<MovieNotFound, bool>> DeleteMovieAsync(int id);
}