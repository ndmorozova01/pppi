using WebApplication.Models.Pets;

namespace WebApplication.Services.Interfaces;

public interface IPetsService
{
    Task<IEnumerable<Pet>> GetAllAsync();

    Task<Pet> AddPetAsync(AddPet addMovie);

    Task<OneOf.OneOf<PetNotFound, Pet>> UpdatePetAsync(int id, UpdatePet updateMovie);

    Task<OneOf.OneOf<PetNotFound, bool>> DeletePetAsync(int id);
}