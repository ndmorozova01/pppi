using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class PasswordService : IPasswordService
{
    public bool VerifyPassword(string password, string hash) => BCrypt.Net.BCrypt.Verify(password, hash);
}