using Microsoft.EntityFrameworkCore;
using OneOf;
using WebApplication.Models.Pets;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services;

public class PetsService : IPetsService
{
    private readonly ApplicationDbContext _context;

    public PetsService(ApplicationDbContext context) => _context = context;

    public async Task<IEnumerable<Pet>> GetAllAsync() => await _context.Pets.AsNoTracking().ToListAsync();

    public async Task<Pet> AddPetAsync(AddPet addPet)
    {
        var pet = new Pet
        {
            FavouriteToy = addPet.FavouriteToy,
            Name = addPet.Name,
            DateOfBirth = addPet.DateOfBirth
        };

        _context.Pets.Add(pet);

        await _context.SaveChangesAsync();
        
        return pet;
    }

    public async Task<OneOf<PetNotFound, Pet>> UpdatePetAsync(int id, UpdatePet updatePet)
    {
        var pet = await _context.Pets.FindAsync(id);

        if (pet == null)
        {
            return new PetNotFound { Id = id };
        }

        if (updatePet.DateOfBirth.HasValue && pet.DateOfBirth != updatePet.DateOfBirth.Value)
        {
            pet.DateOfBirth = updatePet.DateOfBirth.Value;
        }

        if (updatePet.Name != null && pet.Name != updatePet.Name)
        {
            pet.Name = updatePet.Name;
        }
        
        if (updatePet.FavouriteToy != null && pet.Name != updatePet.FavouriteToy)
        {
            pet.FavouriteToy = updatePet.FavouriteToy;
        }

        await _context.SaveChangesAsync();

        return pet;
    }

    public async Task<OneOf<PetNotFound, bool>> DeletePetAsync(int id)
    {
        var pet = await _context.Pets.FindAsync(id);

        if (pet == null)
        {
            return new PetNotFound { Id = id };
        }

        var result = _context.Pets.Remove(pet);

        await _context.SaveChangesAsync();

        return result.State == EntityState.Deleted;
    }
}