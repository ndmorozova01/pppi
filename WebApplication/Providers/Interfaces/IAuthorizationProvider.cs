using OneOf;
using WebApplication.Models.Authorization;

namespace WebApplication.Providers.Interfaces;

public interface IAuthorizationProvider
{
    Task<OneOf<InvalidCredentials, Tokens>> GetTokens(AuthUser authUser);

    Task<OneOf<InvalidRefreshToken, Tokens>> RefreshToken(string refreshToken);
    
    Task<OneOf<InvalidRefreshToken, bool>> RevokeToken(string refreshToken);
}