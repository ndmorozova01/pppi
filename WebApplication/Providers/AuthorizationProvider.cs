using OneOf;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using WebApplication.Models.Authorization;
using WebApplication.Providers.Interfaces;
using WebApplication.Services.Interfaces;

namespace WebApplication.Providers;

public class AuthorizationProvider : IAuthorizationProvider
{
    private readonly IPasswordService _passwordService;
    private readonly IUsersService _usersService;
    private readonly ITokensService _tokensService;
    private readonly IConfiguration _configuration;

    public AuthorizationProvider(IPasswordService passwordService,
        IUsersService usersService,
        ITokensService tokensService,
        IConfiguration configuration)
    {
        _passwordService = passwordService;
        _usersService = usersService;
        _tokensService = tokensService;
        _configuration = configuration;
    }

    public async Task<OneOf<InvalidCredentials, Tokens>> GetTokens(AuthUser authUser)
    {
        var user = await _usersService.GetByEmail(authUser.Email);

        if (user == null)
        {
            return new InvalidCredentials { Email = authUser.Email };
        }

        if (!_passwordService.VerifyPassword(authUser.Password, user.Password))
        {
            return new InvalidCredentials { Email = authUser.Email };
        }

        var tokens = new Tokens
        {
            AccessToken = GetAccessToken(user),
            RefreshToken = GetRefreshToken(user),
            UserEmail = user.Email
        };

        await _tokensService.AddTokensAsync(tokens);

        return tokens;
    }

    public async Task<OneOf<InvalidRefreshToken, Tokens>> RefreshToken(string refreshToken)
    {
        var tokens = await _tokensService.GetTokensAsync(refreshToken);

        if (tokens == null)
        {
            return new InvalidRefreshToken { RefreshToken = refreshToken };
        }

        var user = await _usersService.GetByEmail(tokens.UserEmail);
        
        if (user == null)
        {
            return new InvalidRefreshToken { RefreshToken = refreshToken };
        } 

        tokens.RefreshToken = GetAccessToken(user);
        tokens.RefreshToken = GetRefreshToken(user);

        await _tokensService.UpdateTokensAsync(tokens);

        return tokens;
    }

    public async Task<OneOf<InvalidRefreshToken, bool>> RevokeToken(string refreshToken) =>
        await _tokensService.RemoveTokens(refreshToken);

    private string GetAccessToken(User user)
    {
        var securityToken = new JwtSecurityToken(
            issuer: _configuration["Jwt:Issuer"],
            audience: _configuration["Jwt:Audience"],
            claims: CreateClaims(user),
            expires: DateTime.UtcNow.AddMilliseconds(_configuration.GetValue<int>("Jwt:Lifetime")),
            signingCredentials: GetSigningCredentials()
        );
        var tokenHandler = new JwtSecurityTokenHandler();
        return tokenHandler.WriteToken(securityToken);
    }

    private string GetRefreshToken(User user)
    {
        var securityToken = new JwtSecurityToken(
            issuer: _configuration["Jwt:Issuer"],
            audience: _configuration["Jwt:Audience"],
            claims: CreateClaims(user),
            signingCredentials: GetSigningCredentials()
        );
        var tokenHandler = new JwtSecurityTokenHandler();
        return tokenHandler.WriteToken(securityToken);
    }

    private SigningCredentials GetSigningCredentials() =>
        new(
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!)),
            SecurityAlgorithms.HmacSha512Signature);

    private Claim[] CreateClaims(User user)
    {
        return new Claim[]
        {
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)),
            new(ClaimTypes.NameIdentifier, user.Email),
            new(ClaimTypes.Name, user.Name),
            new(ClaimTypes.Email, user.Email)
        };
    }
}