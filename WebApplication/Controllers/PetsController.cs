using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Pets;
using WebApplication.Services.Interfaces;

namespace WebApplication.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class PetsController : ControllerBase
{
    private readonly IPetsService _petsService;

    public PetsController(IPetsService petsService)
    {
        _petsService = petsService;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllAsync() => Ok(await _petsService.GetAllAsync());

    [HttpPost]
    public async Task<IActionResult> AddPetAsync([FromBody] AddPet addMovie) =>
        Ok(await _petsService.AddPetAsync(addMovie));

    [Route("{id:int}")]
    [HttpPut]
    public async Task<IActionResult> UpdatePetAsync(int id, [FromBody] UpdatePet updateMovie)
    {
        var result = await _petsService.UpdatePetAsync(id, updateMovie);
        return result.Match<IActionResult>(mnf => NotFound(mnf), m => Ok(m));
    }

    [Route("{id:int}")]
    [HttpDelete]
    public async Task<IActionResult> DeletePetAsync(int id)
    {
        var result = await _petsService.DeletePetAsync(id);
        return result.Match<IActionResult>(mnf => NotFound(mnf), m => Ok(new { deleted = m }));
    }
}