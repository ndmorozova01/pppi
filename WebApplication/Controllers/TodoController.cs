using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Todos;
using WebApplication.Services.Interfaces;

namespace WebApplication.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class TodoController : ControllerBase
{
    private readonly ITodoService _petsService;

    public TodoController(ITodoService petsService)
    {
        _petsService = petsService;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllAsync() => Ok(await _petsService.GetAllAsync());

    [HttpPost]
    public async Task<IActionResult> AddTodoAsync([FromBody] AddTodo addMovie) =>
        Ok(await _petsService.AddTodoAsync(addMovie));

    [Route("{id:int}")]
    [HttpPut]
    public async Task<IActionResult> UpdateTodoAsync(int id, [FromBody] UpdateTodo updateMovie)
    {
        var result = await _petsService.UpdateTodoAsync(id, updateMovie);
        return result.Match<IActionResult>(mnf => NotFound(mnf), m => Ok(m));
    }

    [Route("{id:int}")]
    [HttpDelete]
    public async Task<IActionResult> DeleteTodoAsync(int id)
    {
        var result = await _petsService.DeleteTodoAsync(id);
        return result.Match<IActionResult>(mnf => NotFound(mnf), m => Ok(new { deleted = m }));
    }
}