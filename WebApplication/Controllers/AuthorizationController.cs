using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Authorization;
using WebApplication.Providers.Interfaces;

namespace WebApplication.Controllers;

[ApiController]
[Route("[controller]")]
public class AuthorizationController : ControllerBase
{
    private readonly IAuthorizationProvider _authorizationProvider;

    public AuthorizationController(IAuthorizationProvider authorizationProvider)
    {
        _authorizationProvider = authorizationProvider;
    }
    
    [HttpPost]
    public async Task<IActionResult> GetAllTokens([FromBody] AuthUser authUser)
    {
        var response = await _authorizationProvider.GetTokens(authUser);
        return response.Match<IActionResult>(i => NotFound(i), r => Ok(r));
    }
    
    [Route("refresh")]
    [HttpPost]
    public async Task<IActionResult> RefreshTokens([FromQuery] string refreshToken)
    {
        var response = await _authorizationProvider.RefreshToken(refreshToken);
        return response.Match<IActionResult>(i => NotFound(i), r => Ok(r));
    }
    
    [Route("revoke")]
    [HttpPost]
    public async Task<IActionResult> RevokeTokens([FromQuery] string refreshToken)
    {
        var response = await _authorizationProvider.RevokeToken(refreshToken);
        return response.Match<IActionResult>(i => NotFound(i), r => Ok(new { revoked = r }));
    }
}