using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Services.Interfaces;

namespace WebApplication.Controllers;

[Authorize]
[ApiController]
[ApiVersion("1.0", Deprecated = true)]
[ApiVersion("2.0")]
[ApiVersion("3.0")]
[Route("[controller]")]
public class VersionedController : ControllerBase
{
    private readonly IVersioningService _versioningService;

    public VersionedController(IVersioningService versioningService)
    {
        _versioningService = versioningService;
    }
    
    [MapToApiVersion("1.0")]
    [HttpGet]
    [Route("v{version:apiVersion}")]
    public async Task<IActionResult> GetV1()
    {
        return Ok(_versioningService.GetV1());
    }
    
    [MapToApiVersion("2.0")]
    [HttpGet]
    [Route("v{version:apiVersion}")]
    public async Task<IActionResult> GetV2()
    {
        return Ok(_versioningService.GetV2());
    }
    
    [MapToApiVersion("3.0")]
    [HttpGet]
    [Route("v{version:apiVersion}")]
    public async Task<IActionResult> GetV3()
    {
        return File(_versioningService.GetV3(), "application/vnd.ms-excel", "test.xlsx");
    }
}