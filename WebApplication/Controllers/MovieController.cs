using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models.Movies;
using WebApplication.Services.Interfaces;

namespace WebApplication.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class MovieController : ControllerBase
{
    private readonly IMoviesService _moviesService;

    public MovieController(IMoviesService moviesService) => _moviesService = moviesService;

    [HttpGet]
    public async Task<IActionResult> GetAllAsync() => Ok(await _moviesService.GetAllAsync());

    [HttpPost]
    public async Task<IActionResult> AddMovieAsync([FromBody] AddMovie addMovie) =>
        Ok(await _moviesService.AddMovieAsync(addMovie));

    [Route("{id:int}")]
    [HttpPut]
    public async Task<IActionResult> UpdateMovieAsync(int id, [FromBody] UpdateMovie updateMovie)
    {
        var result = await _moviesService.UpdateMovieAsync(id, updateMovie);
        return result.Match<IActionResult>(mnf => NotFound(mnf), m => Ok(m));
    }

    [Route("{id:int}")]
    [HttpDelete]
    public async Task<IActionResult> DeleteMovieAsync(int id)
    {
        var result = await _moviesService.DeleteMovieAsync(id);
        return result.Match<IActionResult>(mnf => NotFound(mnf), m => Ok(new { deleted = m }));
    }
}