namespace WebApplication.Models.Todos;

public class AddTodo
{
    public DateTime CreateDate { get; set; }

    public string Title { get; set; }

    public bool IsDone { get; set; }
}