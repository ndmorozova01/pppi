using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Models.Todos;

[Table("todos")]
public class Todo
{
    [Key]
    [Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Column("createDate")]
    public DateTime CreateDate { get; set; }

    [Column("title")]
    public string Title { get; set; }

    [Column("isDone")]
    public bool IsDone { get; set; }
}