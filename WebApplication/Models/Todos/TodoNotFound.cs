namespace WebApplication.Models.Todos;

public class TodoNotFound
{
    public int Id { get; set; }
}