namespace WebApplication.Models.Movies;

public class AddMovie
{
    public string Title { get; set; }

    public DateOnly ReleasedAt { get; set; }
    
    public Genre Genre { get; set; }
}