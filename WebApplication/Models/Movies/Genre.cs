namespace WebApplication.Models.Movies;

public enum Genre
{
    ScienceFiction,
    Drama,
    History
}