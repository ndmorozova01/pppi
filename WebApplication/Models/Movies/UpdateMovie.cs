namespace WebApplication.Models.Movies;

public class UpdateMovie
{
    public string? Title { get; set; }

    public DateOnly? ReleasedAt { get; set; }
    
    public Genre? Genre { get; set; }
}