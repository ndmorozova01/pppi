using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Models.Movies;

[Table("movies")]
public class Movie
{
    [Key]
    [Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    
    [Column("title")]
    public string Title { get; set; }

    [Column("releasedAt")]
    public DateOnly ReleasedAt { get; set; }
    
    [Column("genre")]
    public Genre Genre { get; set; }
}