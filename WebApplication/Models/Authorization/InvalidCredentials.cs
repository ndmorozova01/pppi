namespace WebApplication.Models.Authorization;

public class InvalidCredentials
{
    public string Email { get; set; }
}