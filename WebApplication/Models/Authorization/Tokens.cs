using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Models.Authorization;

[Table("tokens")]
public class Tokens
{
    [Key]
    [Column("userEmail")]
    public string UserEmail { get; set; }

    [Column("refreshToken")]
    public string RefreshToken { get; set; }

    [Column("accessToken")]
    public string AccessToken { get; set; }
}