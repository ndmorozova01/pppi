namespace WebApplication.Models.Authorization;

public class InvalidRefreshToken
{
    public string RefreshToken { get; set; }
}