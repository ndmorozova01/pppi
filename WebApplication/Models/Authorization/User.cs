using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Models.Authorization;

[Table("users")]
public class User
{
    [Column("name")]
    public string Name { get; set; }

    [Column("surname")]
    public string Surname { get; set; }

    [Key]
    [Column("email")]
    public string Email { get; set; }

    [Column("dateOfBirth")]
    public DateOnly DateOfBirth { get; set; }

    [Column("password")]
    public string Password { get; set; }
}