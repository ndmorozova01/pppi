using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Models.Pets;

[Table("pets")]
public class Pet
{
    [Key]
    [Column("id")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Column("dateOfBirth")]
    public DateOnly DateOfBirth { get; set; }

    [Column("name")]
    public string Name { get; set; }

    [Column("favouriteToy")]
    public string FavouriteToy { get; set; }
}