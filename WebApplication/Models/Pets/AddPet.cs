namespace WebApplication.Models.Pets;

public class AddPet
{
    public DateOnly DateOfBirth { get; set; }

    public string Name { get; set; }

    public string FavouriteToy { get; set; }
}