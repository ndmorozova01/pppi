using Microsoft.EntityFrameworkCore;
using WebApplication.Models.Authorization;
using WebApplication.Models.Movies;
using WebApplication.Models.Pets;
using WebApplication.Models.Todos;

namespace WebApplication;

public sealed class ApplicationDbContext : DbContext
{
    public DbSet<Todo> Todos { get; set; }

    public DbSet<Pet> Pets { get; set; }

    public DbSet<Movie> Movies { get; set; }

    public DbSet<User> Users { get; set; }
    
    public DbSet<Tokens> Tokens { get; set; }

    public ApplicationDbContext(DbContextOptions options) : base(options)
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Todo>().HasData(DataSeeder.Todos);
        modelBuilder.Entity<Movie>().HasData(DataSeeder.Movies);
        modelBuilder.Entity<Pet>().HasData(DataSeeder.Pets);
        modelBuilder.Entity<User>().HasData(DataSeeder.Users);
    }
}