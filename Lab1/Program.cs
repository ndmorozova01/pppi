﻿using Lab1;
using System.CommandLine;

var loremCommand = new Command("lorem", "Displays 'lorem' in console.");
loremCommand.SetHandler(() => Console.WriteLine(File.ReadAllText("Lorem.txt")));

var leftOption = new Option<int>("--left", "First argument");
var rightOption = new Option<int>("--right", "Second argument");
var operationOption = new Option<Operation>("--operation", "Operation type");

var mathCommand = new Command("math", "Math functions.")
{
    leftOption,
    rightOption,
    operationOption
};

mathCommand.SetHandler((left, right, operation) =>
{
    var result = operation switch
    {
        Operation.Multiply => left * right,
        Operation.Subtract => left - right,
        Operation.Sum => left + right
    };

    var operationSymbol = operation switch
    {
        Operation.Multiply => '*',
        Operation.Subtract => '-',
        Operation.Sum => '+'
    };

    Console.WriteLine($"{left} {operationSymbol} {right} = {result}");
}, leftOption, rightOption, operationOption);

var rootCommand = new RootCommand("Lab 1 functions");

rootCommand.AddCommand(loremCommand);
rootCommand.AddCommand(mathCommand);

// Call 1: math --left 5 --right 10 --operation Sum
// Call 2: lorem

return await rootCommand.InvokeAsync(args);