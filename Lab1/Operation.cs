namespace Lab1;

public enum Operation
{
    Sum,
    Multiply,
    Subtract
}