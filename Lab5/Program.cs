﻿using Lab5;

const string apiKey = "8a6e50e02076a6fbf2e89472c5c77873";
const string url = "https://api.themoviedb.org/3/movie";

var moviesClient = new MoviesClient(apiKey, url);

var movies = await moviesClient.GetMovies();
Console.WriteLine($"Total number of movies: {movies!.total_results}");

var ratedMovie = await moviesClient.RateMovie(movies.results[0].id, 8.6);

if (ratedMovie.IsSuccessStatusCode)
{
    var response = await ratedMovie.Content.ReadAsStringAsync();
    Console.WriteLine(response);
}
else
{
    Console.WriteLine("Failed to call API");
}