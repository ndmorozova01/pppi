using System.Net;
using System.Text;
using System.Text.Json;

namespace Lab5;

public class MoviesClient : IDisposable
{
    private readonly string _apiKey;
    private readonly string _uri;
    private readonly HttpClient _httpClient;

    public MoviesClient(string apiKey, string uri)
    {
        _apiKey = apiKey;
        _uri = uri;
        _httpClient = new HttpClient();
        _httpClient.DefaultRequestHeaders.Add("Authorization",_apiKey);
    }

    public async Task<Root?> GetMovies()
    {
        try
        {
            var url = $"{_uri}/popular?api_key={_apiKey}";
            var httpResponse = await _httpClient.GetAsync(url);
            httpResponse.EnsureSuccessStatusCode();
            httpResponse.StatusCode = HttpStatusCode.OK;
                    
            var responseString = await httpResponse.Content.ReadAsStringAsync();
            
            return JsonSerializer.Deserialize<Root>(responseString);
        }
        catch (HttpRequestException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            return null;
        }
    }

    public async Task<HttpResponseMessage> RateMovie(int movieId, double rating)
    {
        var url = $"{_uri}/{movieId}/rating?api_key={_apiKey}";
        
        var data = new
        {
            value = rating
        };

        var json = JsonSerializer.Serialize(data);
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        return await _httpClient.PostAsync(url, content);
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}