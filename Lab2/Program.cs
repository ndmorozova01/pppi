﻿using Lab2;
using Lab2.Libraries;

var libraries = new IILibraryExample[]
{
    new NewtonsoftLibrary(),
    new OneOfLibrary(),
    new ConfigurationLibrary(),
    new SerilogLibrary(),
    new RefitLibrary()
};


foreach (var library in libraries)
{
    Console.WriteLine($"Using library {library.Library}:");
    await library.UseAsync();
    Console.WriteLine();
}