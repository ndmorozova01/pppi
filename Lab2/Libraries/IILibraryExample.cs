namespace Lab2;

public interface IILibraryExample
{
    public string Library { get; }

    Task UseAsync();
}