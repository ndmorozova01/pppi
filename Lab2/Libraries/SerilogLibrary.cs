using Serilog;

namespace Lab2.Libraries;

public class SerilogLibrary : IILibraryExample
{
    public string Library => nameof(SerilogLibrary);

    public Task UseAsync()
    {
        var position = new { Latitude = 25, Longitude = 134 };
        var elapsedMs = 34;

        var log = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .CreateLogger();

        log.Information("Processed {@Position} in {Elapsed:000} ms.", position, elapsedMs);
        
        return Task.CompletedTask;
    }
}