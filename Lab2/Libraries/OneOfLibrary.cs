using OneOf;

namespace Lab2.Libraries;

public class OneOfLibrary : IILibraryExample
{
    public string Library => nameof(OneOfLibrary);
    
    public Task UseAsync()
    {
        var random = new Random();

        OneOf<int, string> result = random.NextDouble() > 0.5 ? 1 : "Hello world";

        result.Switch(
            n => Console.WriteLine($"Random value is less than 0.5. It is equal to {n}"),
            s => Console.WriteLine($"Random value is greater than 0.5. It is equal to '{s}'"));

        return Task.CompletedTask;
    }
}