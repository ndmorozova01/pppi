using Newtonsoft.Json;
using Refit;

namespace Lab2.Libraries;

public interface IUsersApi
{
    [Get("/api")]
    Task<Root> GetUsers();
}

public class RefitLibrary : IILibraryExample
{
    public string Library => nameof(RefitLibrary);

    public async Task UseAsync()
    {
        var usersClient = RestService.For<IUsersApi>("https://randomuser.me");
        var results = await usersClient.GetUsers();
        Console.WriteLine(JsonConvert.SerializeObject(results));
    }
}