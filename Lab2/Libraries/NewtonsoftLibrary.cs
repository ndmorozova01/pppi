using Newtonsoft.Json;

namespace Lab2.Libraries;

public class NewtonsoftLibrary : IILibraryExample
{
    public string Library => nameof(NewtonsoftLibrary);

    public Task UseAsync()
    {
        var item = new { User = "Nadja", Language = "C#" };
        Console.WriteLine(JsonConvert.SerializeObject(item));
        return Task.CompletedTask;
    }
}