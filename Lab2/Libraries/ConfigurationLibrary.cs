using Microsoft.Extensions.Configuration;

namespace Lab2.Libraries;

public class ConfigurationLibrary : IILibraryExample
{
    public string Library => nameof(ConfigurationLibrary);

    public Task UseAsync()
    {
        var configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", false, true)
            .Build();

        Console.WriteLine($"Hello: {configuration["hello"]}, Number: {configuration["num"]}");
        
        return Task.CompletedTask;
    }
}