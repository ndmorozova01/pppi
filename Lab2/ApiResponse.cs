namespace Lab2;

public class Name
{
    public string title { get; set; }
    public string first { get; set; }
    public string last { get; set; }
}

public class Result
{
    public string gender { get; set; }
    public Name name { get; set; }
    public string email { get; set; }
    public string phone { get; set; }
    public string cell { get; set; }
    public string nat { get; set; }
}

public class Root
{
    public List<Result> results { get; set; }
}