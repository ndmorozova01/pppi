﻿using Lab4;
using System.Reflection;

var person = new Person("Nadja", "Morozova", 18, new DateTime(2003, 1, 1), "Ukraine");
var type = typeof(Person);
var typeInfo = type.GetTypeInfo();

Console.WriteLine($"Public: {typeInfo.IsPublic}");

var members = type.GetMembers();
foreach (var member in members)
{
    Console.WriteLine(member.Name);
}

var field = type.GetField("_surname", BindingFlags.NonPublic | BindingFlags.Instance)!;

Console.WriteLine($"Field name: {field.Name}");
Console.WriteLine($"Field type: {field.FieldType}");
Console.WriteLine($"Field value: {field.GetValue(person)}");
        
var method = type.GetMethod("GetFullName")!;
Console.WriteLine($"Full name: {method.Invoke(person, null)}");
