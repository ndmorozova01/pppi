using System.Text.Json;

namespace Lab4;

public class Person
{
    public string _firstName;
    private string _surname;
    private int _age;
    protected DateTime _dateOfBirth;
    public string _country;

    public Person(string firstName, string surname, int age, DateTime dateOfBirth, string country)
    {
        _firstName = firstName;
        _surname = surname;
        _age = age;
        _dateOfBirth = dateOfBirth;
        _country = country;
    }
    
    public string ToJson() => JsonSerializer.Serialize(this);

    public int GetAge() => _age;

    public string GetFullName() => $"{_firstName} {_surname}";
}